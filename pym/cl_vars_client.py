#-*- coding: utf-8 -*-

# Copyright 2010 Calculate Ltd. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

#Допустимые ключи 
#   mode -     режим переменной r-не переназначается из командной строки,
#              w-переназначается из командной строки
#   type     - тип переменной состоит из двух элементов(что это и для чего
#              это)
#   value    - значение переменной по умолчанию
#   hide - флаг того, что данная переменная служебная и не отображается
#              при печати списка значений переменных

from cl_client import __version__
from cl_client import __app__

class Data:
    # имя программы
    cl_name = {'value':__app__}

    # версия программы
    cl_ver = {'value':__version__}

    # variable for calculate-client and calculate-desktop packages
    # ip or domain name of CDS
    cl_remote_host = {'mode':'r'}

    # remote host from /proc/cmdline param domain
    cl_remote_host_live = {'mode':'r'}

    #Логин пользователя
    ur_login = {'mode':"r"}

    #Название группы пользователя
    ur_group = {'mode':"r"}

    #Полное имя пользователя
    ur_fullname = {'mode':"r"}

    # Jabber ID пользователя
    ur_jid = {'mode':"r"}

    # Почтовый адрес пользователя
    ur_mail = {'mode':"r"}

    # Домашняя директория пользователя
    ur_home_path = {'mode':"r"}

    # Режим работы клиента
    # (local или имя хоста клиента)
    os_remote_auth = {'mode':'w'}

    # Версия программы которой были наложены шаблоны
    os_remote_client = {'mode':'w'}

    #Пароль пользователя client
    cl_remote_pw = {'mode':'w'}

    # Директории которые не переносятся в ~/Moved
    cl_moved_skip_path = {'value':['Disks','Home','Moved','FTP','Desktop',
                                   'Share']}

    # Директории или файлы которые не переносятся
    # при синхронизации профиля пользователя
    cl_sync_skip_path = {'value':[".googleearth", "Home", "Disks", "FTP",
        'Share', ".local/share/akonadi/db_data", ".VirtualBox",
        ".mozilla/firefox/calculate.default/urlclassifier3.sqlite",
        ".local/share/mime/mime.cache", ".gvfs",
        ".kde4/share/apps/nepomuk/repository/main/data", ".logout",
        ".Xauthority", ".thumbnails", ".mozilla/firefox/*/Cache",
        ".kde4/socket-*", ".cache/", ".local/share/Trash"]}

    # Директории или файлы которые удаляются
    # при синхронизации профиля пользователя
    cl_sync_del_path = {'value':[".kde4/share/config/phonondevicesrc",
                                 ".kde4/cache-*", ".kde4/tmp-*"]}

    # переключатель для шаблонов merge
    ac_client_merge = {}

    # переключатель для шаблонов входа и выхода из домена
    ac_client_domain = {}

    # One profile for all OS 
    cl_profile_all_set = {'mode':'w', 'value':'off'}

    # lib vars
    cl_env_path = {}
    cl_env_server_path = {}
    os_linux_shortname = {}
    os_net_domain = {'mode':"w"}
    
