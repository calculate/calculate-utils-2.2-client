#-*- coding: utf-8 -*-

# Copyright 2010 Calculate Ltd. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

from cl_vars_share import varsShare, clLocale

class fillVars(varsShare):
    """Методы определения значений переменных шаблона"""

    def get_ac_client_domain(self):
        """переключатель для шаблонов входа и выхода из домена"""
        ret = ""
        action = self.Get("cl_action")
        remoteHost = self.Get("cl_remote_host")
        remoteAuth = self.Get("os_remote_auth")
        if action == "domain":
            ret = "up"
        elif action in ("undomain", "uninstall"):
            ret = "down"
        elif action in ("install", "merge"):
            if remoteHost and remoteAuth:
                ret = "up"
            else:
                ret = "down"
        return ret

    def get_ac_client_merge(self):
        """переключатель для шаблонов merge"""
        ret = ""
        action = self.Get("cl_action")
        remoteHost = self.Get("cl_remote_host")
        remoteAuth = self.Get("os_remote_auth")
        if action in ("merge","install","domain","undomain"):
            ret = "up"
        return ret

    def get_cl_remote_host_live(self):
        return self.getValueFromCmdLine("calculate","domain") or ""

    def get_cl_remote_pw(self):
        return self.getValueFromCmdLine("calculate","domain_pw") or ""
