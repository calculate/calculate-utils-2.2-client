#-*- coding: utf-8 -*-

# Copyright 2010 Calculate Ltd. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

from cl_client import client, __app__, __version__
from cl_opt import opt
import sys
from cl_share_cmd import share_cmd
from cl_print import color_print

# Перевод сообщений для программы
from cl_lang import lang
lang().setLanguage(sys.modules[__name__])

# Использование программы
USAGE = _("%prog [options]")

# Описание программы (что делает программа)
DESCRIPTION = _("Change the user password")

# Коментарии к использованию программы
COMMENT_EXAMPLES = _("change the user password for Samba and Unix services")

# Пример использования программы
EXAMPLES = _("%prog")

class passwd_cmd(share_cmd):
    def __init__(self):
        # Объект опций командной строки
        self.optobj = opt(\
            package=__app__,
            version=__version__,
            usage=USAGE,
            examples=EXAMPLES,
            comment_examples=COMMENT_EXAMPLES,
            description=DESCRIPTION,
            option_list=opt.variable_control+opt.color_control,
            check_values=self.checkOpts)
        # Создаем объект логики
        self.logicObj = client()
        # Создаем переменные
        self.logicObj.createClVars()

    def checkOpts(self, optObj, args):
        """Проверка опций командной строки"""
        if args:
            errMsg = _("invalid argument") + ":" + " %s" %" ".join(args)
            self.optobj.error(errMsg)
            return False
        if not optObj.v:
            if optObj.filter:
                errMsg = _("incorrect option") + ":" + " %s" %"--filter" +\
                ": " + _("used with option '-v'")
                self.optobj.error(errMsg)
                return False
            if optObj.xml:
                errMsg = _("incorrect option") + ":" + " %s" %"--xml" +\
                ": " + _("used with option '-v'")
                self.optobj.error(errMsg)
                return False
        return optObj, args

    def setUserPasswordToServer(self):
        """Изменение пароля пользователя на сервере"""
        try:
            res = self.logicObj.setUserPasswordToServer()
        except KeyboardInterrupt:
            sys.stdout.write("\b\b\n")
            color_print().printERROR(_("Manually interrupted"))
            return False
        except EOFError:
            sys.stdout.write("\n")
            color_print().printERROR(_("Manually interrupted"))
            return False
        return res
