#-*- coding: utf-8 -*-

# Copyright 2010 Calculate Ltd. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
from cl_client import client, __app__, __version__
from cl_opt import opt
import sys
from cl_share_cmd import share_cmd

# Перевод сообщений для программы
from cl_lang import lang
lang().setLanguage(sys.modules[__name__])

# Использование программы
USAGE = _("%prog [options] domain")

# Коментарии к использованию программы
COMMENT_EXAMPLES = _("Add settings for connecting to "
                     "domain server.calculate.ru")

# Пример использования программы
EXAMPLES = _("%prog server.calculate.ru")

# Описание программы (что делает программа)
DESCRIPTION = _("Changes settings for connecting to the domain")

# Опции командной строки
CMD_OPTIONS = [{'shortOption':"r",
                'help':_("remove the settings for connecting to the domain")},
               {'longOption':"mount",
                'help':_("mount the [remote] domain resource")},
               {'longOption':"set"},
               {'longOption':"install",
                'help':_("install the package")},
               {'longOption':"uninstall",
                'help':_("uninstall the package")}]

class client_cmd(share_cmd):
    def __init__(self):
        # Объект опций командной строки
        setpos = \
            filter(lambda x:x[1].get('longOption')=="set",
            enumerate(CMD_OPTIONS))[0][0]
        CMD_OPTIONS[setpos] = opt.variable_set[0]
        self.optobj = opt(\
            package=__app__,
            version=__version__,
            usage=USAGE,
            examples=EXAMPLES,
            comment_examples=COMMENT_EXAMPLES,
            description=DESCRIPTION,
            option_list=CMD_OPTIONS + opt.variable_view+opt.color_control,
            check_values=self.checkOpts)
        # Создаем объект логики
        self.logicObj = client()
        # Создаем переменные
        self.logicObj.createClVars()
        # Названия несовместимых опций
        self.optionsNamesIncompatible = ["r", "mount", "install", "uninstall"]
        # Названия опций несовмесимых с именем домена
        self.optionsNamesNotDomain = self.optionsNamesIncompatible

    def getOptionsNotDomain(self, optObj):
        """Получаем опции несовместимые с именем домена"""
        retList = []
        for nameOpt in self.optionsNamesNotDomain:
            retList.append(getattr(optObj, nameOpt))
        return retList

    def _getNamesAllSetOptions(self):
        """Выдает словарь измененных опций"""
        setOptDict = self.optobj.values.__dict__.items()
        defaultOptDict = self.optobj.get_default_values().__dict__.items()
        return dict(set(setOptDict) - set(defaultOptDict)).keys()

    def getStringIncompatibleOptions(self):
        """Форматированная строка несовместимых опций разделенных ','"""
        listOpt = list(set(self.optionsNamesIncompatible) &\
                       set(self._getNamesAllSetOptions()))
        return ", ".join(map(lambda x: len(x) == 1 and "'-%s'"%x or "'--%s'"%x,\
                             listOpt))

    def checkOpts(self, optObj, args):
        """Проверка опций командной строки"""
        optionsNotDomain = self.getOptionsNotDomain(optObj)
        if not args:
            options = optionsNotDomain + [optObj.color, optObj.v,
                                          optObj.filter, optObj.xml]
            if not filter(lambda x: x, options):
                errMsg = _("no such argument")+":"+" %s" %USAGE.split(" ")[-1]
                self.optobj.error(errMsg)
                return False
            elif len(filter(lambda x: x, optionsNotDomain))>1:
                errMsg = _("incompatible options")+":"+" %s"\
                            %self.getStringIncompatibleOptions()
                self.optobj.error(errMsg)
                return False
        elif filter(lambda x: x, optionsNotDomain):
            errMsg = _("unnecessary argument")+":"+" %s" %USAGE.split(" ")[-1]
            self.optobj.error(errMsg)
            return False
        if len(args)>1:
            errMsg = _("incorrect argument") + ":" + " %s" %" ".join(args)
            self.optobj.error(errMsg)
            return False
        if not optObj.v:
            if optObj.filter:
                errMsg = _("incorrect option") + ":" + " %s" %"--filter" +\
                ": " + _("used with option '-v'")
                self.optobj.error(errMsg)
                return False
            if optObj.xml:
                errMsg = _("incorrect option") + ":" + " %s" %"--xml" +\
                ": " + _("used with option '-v'")
                self.optobj.error(errMsg)
                return False
        return optObj, args

    def addDomain(self, domainName):
        """Ввод в домен"""
        if domainName:
            return self.logicObj.addDomain(domainName)
        else:
            self.printERROR(_("No domain name found as argument"))
            return False

    def delDomain(self):
        """Вывод из домена"""
        return self.logicObj.delDomain()

    def mountRemote(self):
        """Монтирование remote и домашней директории если компьютер в домене

        а так-же ввод в домен если найдено имя хоста и пароль для подключения
        """
        return self.logicObj.mountRemote()

    def install(self):
        """Инсталяция программы"""
        return self.logicObj.installProg()

    def updateEnvFiles(self):
        """Апдейт env файлов до новой версии"""
        return self.logicObj.updateEnvFiles()

    def uninstall(self):
        """Удаление программы"""
        return self.logicObj.uninstallProg()
